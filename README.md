# Services Infrastructure Declaration Format Specification

This is an attempt to build a standard to declare an infrastructure.

For now, this work is done in the context of Ansible.

Until version 1.0 is implemented, the specification doesn't follow sementic versionning. Meaning anything could break.

This repository contains :
* the [specification](specification.md)
* some [example declarations](example_declarations)
* some [thoughts](thoughts.md), that contains some futur evolutions.
* some [past thoughts](past-thoughts.md)

