% Thoughts

This file is about future evolution that could come into the specification.

# Evolutions of service keys

## Network_resolution

`url` and `network_paths` are both about network resolution, maybe do the following :
  - include `network_paths` in `network_resolution`
  - move everything from `url` to `network_resolution` ?
```
services:
  <service_name>:
    network_resolution
      scheme:
      hostname:
      port:
      network_paths:
```

## Backup

Policies :
  Frequency
  Full / Incremental
  Location (geographical, organisation, provider, ...)
  Replication
  Pruning

## ACL

Qui peut accéder au service, et depuis où ?

## Monitoring

Permettre de configurer le monitoring :
  external-blackbox : Configure le monitoring d'un point de vue consommateur
  internal-blackbox : Configure le monitoring d'un point de vue interne. Permet de distinguer les problèmes réseaux internes des problèmes réseaux externes.
  whitebox : Configure le monitoring depuis l'application. Si le monitoring externe indique que le service est down alors que depuis l'intérieur le site est up, cela permet de diagnostiquer rapidement si c'est un problème du service ou du réseau
  alerting:
    - <urgency>: niveau d'alertes
    - none : Désactive les alertes de certains sites (ex: les sites de dev).
  none : Par defaut, désactive le monitoring

    monitoring: listOf (internal (internal blackbox) | external (external blackbox) | none[default] )
    backup:


## Backup


## Auth
Configure la méthode d'authentification au service ? Ou juste une string de documentation qui pourra être utilisé pour générer un rapport ?

# Evolution of global keys

New keys: jobs / cronjobs

# Evolution of network_paths

## Allow referencing subpaths
Example:

```
network_paths:
  path1:
  - loadbalancer
  - test_cluster
  path2:
  - router
    interface: interface1
  - path1
  path3:
  - router1
    interface: interface2
  - path1
```
Different example could be when some long paths are used.

## Allow referencing multiple interfaces

Example:
```
network_paths:
  from_router:
  - router:
    interfaces:
      - "interface 1"
      - "interface 2"
  - web_host


```

# network_host other types
bastion ssh is a network_host ?
identification proxy
service mesh
api gateway


## Get default port from scheme in ansible

defaultPorts:
  http: "80"
  https: "443"
  ldap: "389"
  ldaps: "636"
  dns: "53"

Get port : {{ item.port | default(defaultPorts[item.scheme]) | default "443" }}

