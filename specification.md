% Services Infrastructure Declaration Format Specification

```
version: 0.0.1

services:
  <service-name>:
    url:
      scheme: "scheme"
      hostnames:
      - "main_hostname" # First item in the list.
      - "alternative_hostname_1"
      - "alternative_hostname_N"
      port: # Default from protocol
    network_paths:
      - <network_path_name> or <network_path_group_name>

network_paths:
  <network_path_name>:
    - <network_host_name_1>: {} # initial_network_host = entrypoint in your network
    - <network_host_name_2>: {} # intermediate_network_host = host modifying the trafic
    - <network_host_name_3>: {} # intermediate_network_host
    - <network_host_name_N>: {} # destination_network_host = last host configured from this specification. Ideally the target host.

network_paths_group:
  <network_path_group_name>:
    - <network_path_name_1>
    - <network_path_name_N>
```

# Services

`services`:
  type: "dictionnary"
  description: "List of services"

`services.<service_name>.url`:
  type: "dictionnary"
  mandatory: "yes"
  description: "describes the URL used to access the service."

`services.<service_name>.url.scheme`:
  type: "string"
  mandatory: "no, defaults to 'HTTPS'."
  example: "'HTTPS', 'SSH', 'LDAPS', ..."
  description: protocol (HTTPS, LDAPS, ...) used."

`services.<service_name>.url.hostnames`:
  type: "list"
  mandatory: "yes"
  description: "List of hostnames to access the service. The first item is mandatory and is the main hostname. Following items are alternate hostnames and are optionals. Depending on the scheme, this should configure HTTP redirects or DNS CNAME."
  example: "google.com"
  thinking: "Some precautions should be taken with certificates for alternative hostnames. Should monitoring be configured per hostname ?"

`services.<service_name>.url.port`:
  type: "string"
  mandatory: "no, default based on scheme."
  description: "Port to access the service."
  example: "'443' for HTTPS"
  thinking: "Could be in a DNS SRV record."

`services.<service_name>.network_paths`:
  type: "list"
  mandatory: "yes"
  description: "List of `network_paths`. Those are defined in the `network_paths` global dictionnary."

# Network Paths

A network path list the network nodes interacting with the datastream and modifying it. This can be a router with NAT/PAT, a loadbalancer, a reverse-proxy, ...

`network_paths`
  type: "dictionnary"
  description: "Contains all the `network_paths` referenced in `services`."

`network_paths.<network_path_name>`
  type: "dictionnary"
  description: "A specific network_path. Can contain any number of entry. The first entry will be the `initial_network_host`, the last entry the `destination_network_host` and any entry in-between are `intermediate_network_host`."
  thinking: "Sometimes, the last entry is not the `destination_network_host` because the network parts after don't need to be referenced. One good example would be a kubernetes cluster, that include an ingress controller (=reverse-proxy) and services (=loadbalancer)."

`network_paths.<network_path_name>.<network_host_name>`:
  type: "dictionnary"
  description: "A `<network_host_name>` should match host/group from ansible. This allows to load related variables. This is a dictionnary so that you can detail some variables specific to the `network_path` (ex: interface in a router."


The final network_host can be a group vip.

Current thinking hasn't yet extended to:
  - multiple initial_network_hosts (for exemple multihoming or external loadbalancer)
  - loadbalancer not last or second-to-last network, because this would involve groups

# Network Paths Groups

`network_paths_group`:
  type: "dictionnary"
  description: "List of Group of network paths for fast reference in services."

`network_paths_groups.<network_paths_group_name>`:
  type: "list"
  example: "
  ```
  openshift_network_path_group:
    - cri_ens-to-openshift
    - cri_ung-to-openshift
    - sia_prod-to-openshift
    - sia_dms-to-openshift
  ```
  This network_path_group allows paths from multiple interfaces of the router to be referenced in on go.
  "
