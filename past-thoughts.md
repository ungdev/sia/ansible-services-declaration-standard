# Network devices responsabilities and required data in SIA current infrastructure

This was used to this about what `network_paths` should contain. This used version 0.1

## sia-router
Responsability : make sure haproxy-external's port forward is configured

Required data :
- Interface (= interface receiving the packets) : From `network_paths: cri_ens-to-openshift: router_sia: receiving_interface`
- Protocol : From `service: url: scheme`
- Source Address : Wildcard except if ACL (maybe other reasons like PBR ?)
- Source Ports : Wildcard except if ACL (maybe other reasons PBR ?)
- Dest. Address (address dest. for the client): From `services: <service_name> url: hostnames[0]`
- Dest. Ports (port dest. for the client): From `services: <service_name> url: port`
- NAT IP (ip dest. after port forward): From `network-device: haproxy-external: hostname`
- NAT Ports (port dest. after port forward): From `network-device: haproxy-external: port`

## haproxy-external
Elements :
- Frontend
  - Listen: ip+port+parameters
  - ACL: acl_name to haproxy regex
  - use_backend : link acl to backend
- Backend

Responsability :
  - Build ACL
  - In frontend declaration: Link ACL to backend in `use_backend`

Required data :
- frontend_name:
    Declaration method: From `haproxy-external.frontend`
    Generated method: Frontend name generated from `scheme`, use <service>.url.scheme
- backend_name:
    Declaration method: From `haproxy-external.backend`
    Generated method: Backend name generated from destinations declaration.
- acl_regex: From `services.<service_name>.url.hostname`
- acl_name: generate from `acl_regex`
